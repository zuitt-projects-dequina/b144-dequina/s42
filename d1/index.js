/*
Introduction to DOM

DOM - has similar concept to CSS
if in CSS, it is treating the elements as box model
but in JavaScript, it treats all elemets as "Objects"

objects in JS - contains the web page element

*/

// Retrieve an element from the webpage (and to use it, we save it to a constant)
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
// "document" this refers to the WHOLE Webpage
// "querySelector" is used to select specific object (HTML element) from the document


/* Alternatively, we can use "getElement functions" to retrive the elements

	document.getElementById('txt-first-name');
	document.getElementByClassName('txt-inputs');
	document.getElementByTagName('input');
*/

// how to target the element? this comes the "event listeners"
// example of an event is: pressing the keys, hovering the mouse, clicking the mouse

const spanFullName = document.querySelector('#span-full-name');

/* 5 Common Event Listeners

	change - An element has been changed (eg. input value)
	click - an element has been clicked (eg. button click)
	load - The browser has finished loading a webpage
	keydown - a keyboard key is pushed
	keyup - A keyboard key is released after being pushed

*/

// Performs an action when an event trigets
txtFirstName.addEventListener('keyup', (event) => {
	// we use the ".innerHTML" function to use the txtFirstName.value to the "spanFullName"
	spanFullName.innerHTML = txtFirstName.value;

	// console.log(event.target);
	// console.log(event.target.value);
})

// output: As you type in the First Name input box, the typed chars show on the Full Name part


txtFirstName.addEventListener('keyup', (event) => {
	// spanFullName.innerHTML = txtFirstName.value;


	// "event.target" contains the element where the event happened
	console.log(event.target);
	// "event.target.value" gets the value of the input object
	console.log(event.target.value);
})

// output: every actions made in the first Name input, is logged on to the console


// Mini Activity
txtLastName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtLastName.value;
})