////////// ACTIVITY ///////////


// Retrieve an element from the webpage (and to use it, we save it to a constant)
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

let fullName = [txtFirstName, txtLastName];

fullName.forEach(name => {
	name.addEventListener('keyup', event => {
		spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
	})
})

